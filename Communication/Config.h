
//-------------------------
// User Set Values
//-------------------------
const int SERIAL_RATE = 115200;  //The baud rate to communicate at
const int WAITOUT_TIME = 50; //Time to wait in milliseconds to get serial data after inital contact

const byte NUMBER_OF_LCDS = 2; //The number of LCD screens you have. 1 indexed

const byte NUMBER_OF_ANALOG_READINGS = 1;  //Number of analog inputs. 1 indexed
//The automatic update is kinda broken at the moment. its best to leave this to false and ask for updates.
const boolean SEND_INPUT_UPDATES_AUTOMATICLY = false;  // Do you want the arduino to send an update if there is a detected change in the inputs?

const boolean FULL_INIT_SATRTUP = true; // Do you want to preform a full startup check on every boot?
const int INIT_DELAY = 1000;  // This is the pause for each step during boot.


//-------------------------
// Error Codes
//-------------------------
// Note, uint_8 is equivilent to byte
const byte OK = 0x00;
const byte COMM_OUT_OF_RANGE = 0x01;
const byte UNRECOGNISED_COMM = 0x02;
const byte TIMED_OUT = 0x03;
const byte BAD_DATA = 0x04;
const byte INVALID_LCD = 0x05;
const byte BAD_RESPONSE = 0x06;


//-------------------------
// Placeholder values
//-------------------------
int lastAnalogReadings[NUMBER_OF_ANALOG_READINGS];
int currentAnalogReadings[NUMBER_OF_ANALOG_READINGS];
byte check;
byte errorNumber;
byte analogChanged[NUMBER_OF_ANALOG_READINGS];
