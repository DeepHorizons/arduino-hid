// Joshua Milas, 2013/03/30  v0.1
// A re-write of a serial based library to communicate with an LCD screen.
// The library was from some dark corner of the interwebs. Cant seem to find my source
// It was called LCD12864RSPI incase anyone wants to look for it

// Set up to communicate with a Newhaven display, NHD-19232WG-BTMI-V#T
// A 192x32 pixed graphic LCD display

// Need to
//   re-wire it to use the SoftwareSerial library.
//      Not sure if this is possible
//   Should set up something for the size of the screen
//   Test and re-write the d_full_screen

#include "Arduino.h"

class LCDSerial {
  public:
  LCDSerial(byte latch, byte data, byte clock);
  
  void init();
  void delayns();
  void write_byte(int dat);
  void write_CMD(int CMD);
  void write_data(int CMD);
  
  void clear();
  
  //Because we love overloading!
  void d_str(int x, int y, char *ptr, int dat); //The original
  void d_str(int x, char *ptr);  //A simplified version of the original
  void d_str(int x, char ptr);   //One that takes one character
  void d_str(int x, String ptr); //One that takes strings
  
  void d_sig(int M, int N, int sig);
  void d_full_screen(char *p);
  
  //A usefull function I found incase anyone needed it
  //Rendered obsolete with strlen(), may remove this later on
  int strLength(char *buffer);
  
  int delayTime;
  
  private:
  byte _latchPin;
  byte _clockPin;
  byte _dataPin;
};

//-------------------------
// Sets up the library, with the latch, data, and clock pins.
//-------------------------
LCDSerial::LCDSerial(byte latch, byte data, byte clock) {
  _latchPin = latch;
  _dataPin = data;
  _clockPin = clock;
  
  delayTime = 80; //Need to define that here, cant do it in the class
}

//-------------------------
// Initilizes the LCD screen
//-------------------------
void LCDSerial::init() {
  pinMode(_latchPin, OUTPUT);
  pinMode(_clockPin, OUTPUT);
  pinMode(_dataPin, OUTPUT);
  delayns();
  
  write_CMD(0x30);  //Display control; Display on, cursor off, character blink off
  write_CMD(0x0C);  //???Return home
  write_CMD(0x01);  //???
  write_CMD(0x06);
}

//-------------------------
// Delay the program delayTime microseconds.
// The delay time is set up in the library class
//-------------------------
void LCDSerial::delayns() {
  delayMicroseconds(delayTime);
}

//-------------------------
// Writes a byte to the screen
//-------------------------
void LCDSerial::write_byte(int CMD) {
  digitalWrite(_latchPin, HIGH);
  delayns();
  shiftOut(_dataPin, _clockPin, MSBFIRST, CMD);
  digitalWrite(_latchPin, LOW);
}

//-------------------------
// Sends a command to the screen
//-------------------------
void LCDSerial::write_CMD(int CMD) {
  int HData, LData;
  HData = CMD;
  HData &= 0xF0;
  LData = CMD;
  LData &= 0x0F;
  LData <<= 4;
  
  write_byte(0xF8);
  write_byte(HData);
  write_byte(LData);
}

//-------------------------
// Sends data to the screen
//-------------------------
void LCDSerial::write_data(int CMD) {
  int HData, LData;
  HData = CMD;
  HData &= 0xF0;
  LData = CMD;
  LData &= 0x0F;
  LData <<= 4;
  
  write_byte(0xFA);
  write_byte(HData);
  write_byte(LData);
}

//-------------------------
// Clear the LCD screen
//-------------------------
void LCDSerial::clear() {
  write_CMD(0x30);
  write_CMD(0x01);
  delayMicroseconds(1600); //The clear function takes 1.6 ms, or 1600 microseconds acording to the Data sheet.
}

//-------------------------
// Display sent data to the screen
// This is an overloaded function that will accept a character array,
// a single character, or a string.
//-------------------------
void LCDSerial::d_str(int x, int y, char *ptr, int dat) {
  int i;
  
  switch(x) {
    case 0: 
      y|=0x80;
      break;
    case 1:
      y|=0x90;
      break;
    case 2:
      y|=0x88;
      break;
    case 3:
      y|=0x98;
      break;
     default:
       break;
  }
  
  write_CMD(y);
  
  for( i=0; i<dat; i++ ) {
    write_data(ptr[i]);
  }
}

void LCDSerial::d_str(int x, char *ptr) {
  int i,y = 0;
  
  switch(x) {
    case 0: 
      y|=0x80;
      break;
    case 1:
      y|=0x90;
      break;
    case 2:
      y|=0x88;
      break;
    case 3:
      y|=0x98;
      break;
     default:
       break;
  }
  
  write_CMD(y);
  
  for( i=0; i < strlen(ptr) ; i++ ) {
    write_data(ptr[i]);
  }
}

void LCDSerial::d_str(int x, char ptr) {
  int i,y = 0;
  
  switch(x) {
    case 0: 
      y|=0x80;
      break;
    case 1:
      y|=0x90;
      break;
    case 2:
      y|=0x88;
      break;
    case 3:
      y|=0x98;
      break;
     default:
       break;
  }
  
  write_CMD(y);
  
  write_data(ptr);
}

void LCDSerial::d_str(int x, String ptr) {
  int i,y = 0;
  
  char blah[ptr.length() + 1];
  ptr.toCharArray(blah, ptr.length()+1);
  
  switch(x) {
    case 0: 
      y|=0x80;
      break;
    case 1:
      y|=0x90;
      break;
    case 2:
      y|=0x88;
      break;
    case 3:
      y|=0x98;
      break;
     default:
       break;
  }
  
  write_CMD(y);
  
  for( i=0; i < strlen(blah) ; i++ ) {
    write_data(blah[i]);
  }
}

//-------------------------
// Displays a special character on the screen
//-------------------------

void LCDSerial::d_sig( int M, int N, int sig) {
  switch(M) {
    case 0:
      N |= 0x80;
      break;
    case 1:
      N |= 0x90;
      break;
    case 2:
      N |= 0x88;
      break;
    case 3:
      N |= 0x98;
      break;
    default:
      break;
  }
  
  write_CMD(N);
  write_data(sig);
}

//-------------------------
// Displays picture on the screen.
// Need to send a character array
//-------------------------
void LCDSerial::d_full_screen(char *p) {
  int ygroup,x,y,i;
  int temp;
  int tmp;
         //The height in pixles
  for(ygroup=0;ygroup<32;ygroup++) {
    if(ygroup<32) { //The height in pixles
      x=0x80;
      y=ygroup+0x80;
    }else{
      x=0x88;
      y=ygroup-32+0x80;   //The height in pixles 
    }         
    write_CMD(0x34);        //Ð´ÈëÀ©³äÖ¸ÁîÃüÁî
    write_CMD(y);           //Ð´ÈëyÖá×ø±ê
    write_CMD(x);           //Ð´ÈëxÖá×ø±ê
    write_CMD(0x30);        //Ð´Èë»ù±¾Ö¸ÁîÃüÁî
    tmp=ygroup*24; //stoped at 17 //This corelates to the way it draws the x
    for(i=0;i<24;i++) { //This corelates to the way it draws the x
      temp=p[tmp++];
      write_data(temp);
    }
  }
  write_CMD(0x34);        //Ð´ÈëÀ©³äÖ¸ÁîÃüÁî
  write_CMD(0x36);        //ÏÔÊ¾Í¼Ïó
}



//-------------------------
// Returns the length of a string or character array
//-------------------------
int strLength(char *buffer){
  int i = 0, length =0;
  while (buffer != '\0'){  
    if (buffer[i+1] == '\0') {
    length = i+1;
    return length;
    }
    i++;
  }
}
  
