/* Joshua Milas 2013/03/19
V0.01 - implimented basic commands
Sketch to provide a front end for any simulator or HID device
anyone can think of
*/

/* TODO
2013/04/15
Add functionality in check_Analog_Readings() to determing what has changed
*/

//---------------
// Include external libraries
//---------------
#include <Wire.h>
#include <inttypes.h>
#include <LCDi2cNHD.h>

//---------------
// Include internal libraries
//---------------
#include "LCD_serial.h"

//---------------
// Include internal functions and configs
//---------------
#include "Config.h"
#include "Func.h"


void setup() {
  //Setup the serial port with the defined rate
  Serial.begin(SERIAL_RATE);
  init_LCD_screens();
  
  //Getting the current analog values and placing them in the last
  for(int i = 0; i < NUMBER_OF_ANALOG_READINGS; i++) {
    lastAnalogReadings[i] = analogRead(i);
  }

}

void loop() {
  //If there is serial data available...
  if( Serial.available() ) {
    //Read it and check if the command is good.
    check = check_Command(Serial.read());
    //Print the error code
    Serial.write(check);
  }
  
  //If there has been a change in the analog readings and se said to send automagic updates...
  if ( check_Analog_Readings() && SEND_INPUT_UPDATES_AUTOMATICLY ) {
    
    //Tell the computer that we got a new set of data
    Serial.write(0x80);
    
    //Now we wait for the computer to aknowlege and say its ready to recieve
    errorNumber = time_out();
    if( errorNumber > OK ) {
      Serial.write(errorNumber);
    }else{
      check = Serial.read();
      if (check != 0x80) {
        //If the computer sent the wrong data, tell it did
        Serial.write(BAD_RESPONSE);
      }
      else{ 
        //If everything went ok...
        //send it
        send_Analog_Readings();
        //Serial.write(OK);
      }
    }
  }
}

int check_Command(int in) {
  //Check to see if code is within correct range
  if (in > 0xFF) {
    //Return the error for incorect range
    return COMM_OUT_OF_RANGE;
  }
	
  switch( in ) {
    case 0x0F: {
      //Heartbeat
      return OK;
      break; }
    case 0x10: {
      //LCD screen update
      
      //We are answering the call
      Serial.write(0x10);
                        
      //The protocol defines we are going to get three segemnts of data
      //The first two sets are hex numbers, declaring which screen and what line to write on
                        
      //Here, we are going to wait till there is data, or until we time out
      //If we time out, return an error code
      errorNumber = time_out();
      if( errorNumber > OK ) return errorNumber;
      //now we read the first data set
      byte LCDSelect = Serial.read();
                        
      //Here, we are going to wait till there is data, or until we time out
      //If we time out, return an error code
      errorNumber = time_out();
      if( errorNumber > OK ) return errorNumber;
      //The next set is the line to write on
      byte LCDLine = Serial.read();
                        
      //Here, we are going to wait till there is data, or until we time out
      //If we time out, return an error code
      errorNumber = time_out();
      if( errorNumber > OK ) return errorNumber;
      //Now we are getting a sring, so we use a function to do that
      String LCDData = read_string();
                        
                        
                        
      //Now that we got the data, Send it over to to a function to see what to do with it.
      errorNumber = LCD_write_data(LCDSelect, LCDLine, LCDData);
      if( errorNumber > OK ) return errorNumber;
                        
      //If everything went ok, then say everything went ok!
      return OK;
      break; }
    case 0x20: {
      //Request update on joystick
      
      //We preform the handshake
      Serial.write(0x20);
      
      //Then we send the data
      send_Analog_Readings();
      return OK;
      break; }
    default: {
      //Not a defined command
      return UNRECOGNISED_COMM;
      break; }
  }
}
