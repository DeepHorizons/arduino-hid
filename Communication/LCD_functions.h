/* LCD Functions 2013/03/20

This file contains functions pertaining to any LCD or display
type output.

*/

LCDi2cNHD charLCD = LCDi2cNHD(2, 20, 0x50>>1, 0);
LCDSerial GLCD(3, 4, 5);

//This is to try and fix timing issues with the I2C LCD screen.
//Its super slow on some functions.
byte lastLCDLine, currentLCDLine = 0;

//-------------------------
// Ininilizing the LCD screens
//-------------------------
void init_LCD_screens() {
  
  //LCD screen #0
  charLCD.init();
  charLCD.setContrast(35);
  charLCD.setBacklight(9);
  charLCD.setDelay(4, 0); //This library sets the delay to 50 miliseconds, this is waaaaay to long for this dislay
  //The longest delay for this display is 4 miliseconds
  
  //Start displaying stuff so we know its working
  if (FULL_INIT_SATRTUP) {
    charLCD.command(0x70);  //This command is for this particular LCD screen. it displays the firmware version.
    delay(INIT_DELAY);
    charLCD.clear();
    charLCD.print("Done Initilizing....");
    charLCD.setCursor(1,0);
    charLCD.print("Done Initilizing....");
    delay(INIT_DELAY);
    charLCD.clear();
  }
  
  
  //LCD screen #1
  GLCD.init();
  //Start displaying stuff so we know its working
  if (FULL_INIT_SATRTUP) {
    GLCD.d_str(0, "Done Initilizing....");
    GLCD.d_str(1, "Done Initilizing....");
    delay(INIT_DELAY);
    GLCD.clear();
  }
  
}

//-------------------------
// Outputing data to the LCD screens
//-------------------------
byte LCD_write_data(byte lcd, byte line, String data) {
  //If you requested a LCD that you currently dont have defined, tell you
  if( lcd >= NUMBER_OF_LCDS) return INVALID_LCD;
  
  switch(lcd) {
    case 0: {
      currentLCDLine = line;
      if( currentLCDLine == lastLCDLine ) {
        charLCD.clear();
      }else{
        lastLCDLine = currentLCDLine;
      }
      charLCD.setCursor(line, 0);
      charLCD.print(data);
      return OK;
      break;
    }
    case 1: {
      currentLCDLine = line;
      if( currentLCDLine == lastLCDLine ) {
        GLCD.clear();
      }else{
        lastLCDLine = currentLCDLine;
      }
      GLCD.d_str(line, data);
      return OK;
      break;
    }
  } 

}
