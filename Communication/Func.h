/* Joshua Milas 2013/03/19

Generic functions used in the program.

Need to organize this better
*/

#include "LCD_functions.h"



//-------------------------
// Time out for waiting for serial data
/*
  Waits for data to become available on the serial port.
  if the time it takes for the data to come exceded the
  waitout time limit, it will return an error code of 0x03.
*/
//-------------------------
byte time_out() {
  //get the current time
  int timeStart = millis();
  //set a variable to get the latest time
  int timeCurrent;
  while( !Serial.available() ){
    //get the latest time
    timeCurrent = millis();
    //if the differece between the start time and hte current time
    //is bigger than the timeout period, exit with an error code
    if( (timeCurrent - timeStart) > WAITOUT_TIME) {
      return TIMED_OUT;
    }
  }
  return OK;
}


//-------------------------
// Read incoming string
/*
  Reads a incoming string on the serial port
  You need this because the arduino can only read one
  character at a time. This function assembles all the
  characters and returns the whole string.
*/
//-------------------------
String read_string() {
  String content = "";
  char character;
  while ( Serial.available() ) {
    character = Serial.read();
    content.concat(character);
  }
  
  return content;
}

//-------------------------
// Checks if the analog readings have changed
/*
  Reads all the analog inputs starting at 0 to the user defined amount
  and compairs it to the last readings. If there is a change, it returns
  a true, otherwise it returns a false
*/
//-------------------------
boolean check_Analog_Readings() {
  //Get all the defined analog inputs and put them into an array
  for(int i = 0; i < NUMBER_OF_ANALOG_READINGS; i++) {
    currentAnalogReadings[i] = map(analogRead(i), 0 , 1023, 0, 255);
  }
  //Compare all the current analog values with the last ones
  for(int i = 0; i < NUMBER_OF_ANALOG_READINGS; i++) {
    //If a value has changed, then say that there was a change and make the current readings the last ones
    if (currentAnalogReadings[i] != lastAnalogReadings[i]) {
      //Make the current reading teh last ones
      for(int i = 0; i < NUMBER_OF_ANALOG_READINGS; i++) {
        lastAnalogReadings[i] = currentAnalogReadings[i];
      }
      
      return true;
    }
  }
  
  return false;
}

//-------------------------
// Sends all the analog values in the array
/*
  Sends all the analog values defined in the array
*/
//-------------------------
void send_Analog_Readings() {
  
  Serial.write((uint8_t*)currentAnalogReadings, NUMBER_OF_ANALOG_READINGS);
  return;
  
}


