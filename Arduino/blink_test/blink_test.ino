// Test sketch by Joshua Milas
// 2/09/2013 v1
// Blink the LED on pin 13 on and off for 1
// sec per state three times. Then fade the
// pin from off to on 3 times.
// It will also communicate the state of the
// LED over the serial port @ 9600 baud.

// The Lenardo has a PWM pin on 13, the older
// ones do not. The older boards will not fade
// pin 13

int LED = 13;  // Declare the LED pin

void setup() {
  Serial.begin(9600);  //Initilize the serial port
  pinMode(LED, OUTPUT);  //Set the LED pin to output
  digitalWrite(LED, LOW);  //Set the pin to LOW (or off)
  Serial.println("LED START");  //Say somethig on the serial port
  delay(1000);  //Wait 1 second
}

void loop() {
  // Blink the LED three times
  for (int i = 0; i < 3; i++){
    digitalWrite(LED, HIGH);  //Set the pin HIGH
    Serial.println("LED on");  //Say the state of the pin
    delay(1000);  //Wait 1 second
    digitalWrite(LED, LOW);  // Set the pin LOW
    Serial.println("LED off");  //Say the state of the pin
    delay(1000);  //Wait 1 second
  }
  
  // Fade the LED pin three times
  for (int i = 0; i < 3; i++){
    
    // Fade up
    for (int analog = 0; analog <= 255; analog++){
      analogWrite(LED, analog);  //PWM the pin to the current analog loop value
      Serial.print("LED fade up: ");  // Print a intro message and...
      Serial.println(analog);  //... finish the line with the value
      delay(2);  //Delay a tiny bit
    }
    
    Serial.println("LED on");
    // Fade down
    for (int analog = 255; analog >= 0; analog--){
      analogWrite(LED, analog);
      Serial.print("LED fade down: ");
      Serial.println(analog);
      delay(2);
    }
  }
  
  Serial.println("LED off");
  // Delay the off for a second
  delay(1000);
  
}



