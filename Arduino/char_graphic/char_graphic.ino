// A test sketch using two LCD displays
// Joshua Milas - 2013/02/28
// This sketch uses both a character & a
// graphical LCD screen, using two different 
// libraries. The character LCD display
// uses i2c, while the graphic LCD display
// uses a serial connection.

// Stuff for the Character LCD
#include <LCDi2cNHD.h>
#include <Wire.h>
#include <inttypes.h>

// Stuff for the Graphic LCD
#include <LCD12864RSPI.h>
#include "Hatsune.h"
#include "Len.h"

unsigned char init1[] = "Init test";
unsigned char text1[] = "some text";
unsigned char text2[] = "Some More Text";
unsigned char text3[] = "abcdefg";
char test[] = "and here";

LCDi2cNHD charLCD = LCDi2cNHD(2, 20, 0x50>>1, 0);

void setup() {
  //setup char lcd
  charLCD.init();
  charLCD.setContrast(35);  //This is found to have a good contrast
  charLCD.setBacklight(9);  //This is found to have a good backlight
  
  //setup graphic LCD
  LCDA.Initialise();
}

void loop() {
  
  LCDA.CLEAR();  // Clear the graphic LCd display
  charLCD.clear();  // Clear the character LCD display
  charLCD.print("Im writing here");
  LCDA.DisplayString(0,0,test,sizeof("and here"));
  delay(3000);
  
  LCDA.CLEAR();
  charLCD.clear();
  charLCD.print("Line one first");
  LCDA.DisplayString(0,0,"Line Two Second",sizeof("Line Two Second"));
  charLCD.setCursor(1,0);
  charLCD.print("Line Three third...");
  LCDA.DisplayString(1,0,"And Now a picture...",sizeof("And Now a picture"));
  delay(3000);
  
  LCDA.CLEAR();
  charLCD.clear();
  charLCD.print("print before pic");
  LCDA.DrawFullScreen(Len);
  charLCD.setCursor(1,0);
  charLCD.print("print after pic");
  delay(3000);
  
}
  
  
