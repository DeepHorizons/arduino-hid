#include <Wire.h>
#include <LCDi2cNHD.h>
#include "LCD_serial.h"

LCDi2cNHD charLCD = LCDi2cNHD(2, 20, 0x50>>1, 0);
LCDSerial GLCD = LCDSerial(3, 4, 5);

String content = "String";
char *dur = "Char Array";
char fru = 'abc';

int i = 0;

void setup() {
  
  /*
  charLCD.init();
  charLCD.setContrast(35);
  charLCD.setBacklight(9);
  charLCD.command(0x70);
  delay(3000);
  charLCD.print("Initilizing...");
  delay(3000);
  */
  
  GLCD.init();
  GLCD.d_str(0,0,"Init...",sizeof("Init..."));
  
  delay(1000);
}

void loop() {
  
  GLCD.d_str(0,0,"Here is me writing",sizeof("Here is me writing"));
  GLCD.d_str(1,0,"with the default",sizeof("with the default"));
  delay(2000);
  GLCD.clear();
  /*
  char test[content.length() + 1];
  content.toCharArray(test, content.length());
  GLCD.d_str(0, test);
  delay(2000);
  GLCD.clear();
  */
  i++;
  content.concat(i);
  GLCD.d_str(0, fru);
  delay(2000);
  GLCD.clear();
  
  GLCD.d_str(0, dur);
  delay(2000);
  GLCD.clear();
  
  GLCD.d_str(0, content);
  delay(2000);
  GLCD.clear();
  
  
}
