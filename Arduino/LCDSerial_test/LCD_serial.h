// Joshua Milas, 2013/03/17
// A re-write of a serial based library to communicate with an LCD screen.
// Need to
//   re-weire it to use the SoftwareSerial library.
//   Add the rest of the functions

#include "Arduino.h"

class LCDSerial {
  public:
  LCDSerial(byte latch, byte data, byte clock);
  
  void init();
  void delayns();
  void write_byte(int dat);
  void write_CMD(int CMD);
  void write_data(int CMD);
  
  void clear();
  //Because we love overloading!
  //The original
  void d_str(int x, int y, char *ptr, int dat);
  //A simplified version
  void d_str(int x, char *ptr);
  
  void d_str(int x, char ptr);
  //One that takes strings
  void d_str(int x, String ptr);
  //void d_sig(int M, int N, int sig);
  //void d_full_screen(uchar *p);
  
  int strLength(char *buffer); // I found this, its here just incase anyone ever needed it. otherwise, use strlen()
  int delayTime;
  
  private:
  byte _latchPin;
  byte _clockPin;
  byte _dataPin;
};

LCDSerial::LCDSerial(byte latch, byte data, byte clock) {
  _latchPin = latch;
  _clockPin = clock;
  _dataPin = data;
  
  delayTime = 80;
}

void LCDSerial::init() {
  pinMode(_latchPin, OUTPUT);
  pinMode(_clockPin, OUTPUT);
  pinMode(_dataPin, OUTPUT);
  delayns();
  
  write_CMD(0x30);  //Display control; Display on, cursor off, character blink off
  write_CMD(0x0C);  //???Return home
  write_CMD(0x01);  //???
  write_CMD(0x06);
}

void LCDSerial::delayns() {
  delayMicroseconds(delayTime);
}

void LCDSerial::write_byte(int CMD) {
  digitalWrite(_latchPin, HIGH);
  delayns();
  shiftOut(_dataPin, _clockPin, MSBFIRST, CMD);
  digitalWrite(_latchPin, LOW);
}

void LCDSerial::write_CMD(int CMD) {
  int HData, LData;
  HData = CMD;
  HData &= 0xF0;
  LData = CMD;
  LData &= 0x0F;
  LData <<= 4;
  
  write_byte(0xF8);
  write_byte(HData);
  write_byte(LData);
}

void LCDSerial::write_data(int CMD) {
  int HData, LData;
  HData = CMD;
  HData &= 0xF0;
  LData = CMD;
  LData &= 0x0F;
  LData <<= 4;
  
  write_byte(0xFA);
  write_byte(HData);
  write_byte(LData);
}

void LCDSerial::clear() {
  write_CMD(0x30);
  write_CMD(0x01);
}

void LCDSerial::d_str(int x, int y, char *ptr, int dat) {
  int i;
  
  switch(x) {
    case 0: 
      y|=0x80;
      break;
    case 1:
      y|=0x90;
      break;
    case 2:
      y|=0x88;
      break;
    case 3:
      y|=0x98;
      break;
     default:
       break;
  }
  
  write_CMD(y);
  
  for( i=0; i<dat; i++ ) {
    write_data(ptr[i]);
  }
}

void LCDSerial::d_str(int x, char *ptr) {
  int i;
  
  write_CMD(0x80);
  
  for( i=0; i < strlen(ptr) ; i++ ) {
    write_data(ptr[i]);
  }
}

void LCDSerial::d_str(int x, char ptr) {
  int i;
  
  write_CMD(0x80);
  
  write_data(ptr);
}

void LCDSerial::d_str(int x, String ptr) {
  int i;
  
  char blah[ptr.length() + 1];
  ptr.toCharArray(blah, ptr.length()+1);
  
  
  write_CMD(0x80);
  
  for( i=0; i < strlen(blah) ; i++ ) {
    write_data(blah[i]);
  }
}

int strLength(char *buffer){
  int i = 0, length =0;
  while (buffer != '\0'){  
    if (buffer[i+1] == '\0') {
    length = i+1;
    return length;
    }
    i++;
  }
}
