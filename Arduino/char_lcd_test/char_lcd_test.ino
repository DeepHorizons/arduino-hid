#include <LCDi2cNHD.h>
#include <Wire.h>
#include <inttypes.h>

int rows = 2;  //Define the number of rows on the display
int cols = 20;  // Defile the number of columbs on the display

LCDi2cNHD lcd = LCDi2cNHD(rows, cols, 0x50>>1, 0); //Declare the class, the last argument is invalid

void setup() {
  // Start the display and clear it
  lcd.init();
  lcd.setContrast(35);
  lcd.setBacklight(9);
  lcd.command(0x70);
  delay(1000);
}

void loop() {
  lcd.print("Brightness Test");
  delay(1000);
  
  for (int i = 1; i < 255; i++){
    lcd.clear();
    lcd.print(i);
    lcd.setBacklight(i);
    delay(100);
  }
  lcd.clear();
}
