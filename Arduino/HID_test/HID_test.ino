joyState_t joySt;		// Joystick state structure
boolean xUp = 0;
boolean yUp = 0;
boolean tUp = 0;

void setup()
{
	pinMode(13, OUTPUT);
 
	joySt.xAxis = 0;
	joySt.yAxis = 64;
	joySt.throttle = 127;
	joySt.buttons = 1;
 
}

void loop() {
  if (!xUp) joySt.xAxis++;
  else {joySt.xAxis--;}
  if (joySt.xAxis == 255) xUp = 1;
  if (joySt.xAxis == 0) xUp = 0;
  
  if (!yUp) joySt.yAxis++;
  else {joySt.yAxis--;}
  if (joySt.yAxis == 255) yUp = 1;
  if (joySt.yAxis == 0) yUp = 0;
  
  if (!tUp) joySt.throttle++;
  else {joySt.throttle--;}
  if (joySt.throttle == 255) tUp = 1;
  if (joySt.throttle == 0) tUp = 0;
  
  joySt.buttons <<= 1;
  if (joySt.buttons == 0) joySt.buttons = 1;
  
  delay(50);
  
  Joystick.setState(&joySt);
}
